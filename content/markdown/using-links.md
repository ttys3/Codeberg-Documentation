---
eleventyNavigation:
  key: UsingLinks
  title: Using Links
  parent: Markdown
  order: 30
---

You can use links to refer to other articles, sections in articles or to other websites.

## Links with description

It is always good for readability to not just paste an url into your text but to provide
a description of your link. Only the description of the link will be in the rendered form of
your text and the link will be added as html-link.

Links with description have the following markup: `[Link description](link-url)`.

For example:

```
[Link to Codeberg](https://codeberg.org/)
```

Gets rendered as:

[Link to Codeberg](https://codeberg.org/)

## Links without description

To add a link using the url withing your text use `<` and `>` to mark the links.
For example, if you want to add to `https://codeberg.org/` add `<https://codeberg.org>` to your
text. This will lead to the following rendering of the link to <https://codeberg.org>.
You can also simply add the link to your text to have the same effect: https://codeberg.org
However it is easier to parse links in the text if the links are explicitly marked by the less
than `<` and greater than `>` characters.

## URIs and URLs

You can link to another article by specifying the file or path name URI (without specifying the protocol part of an URL).

For example, you can link to the introductory article of this section of the documentation by using its
path name in the link:

```
[Link to Introductory article](/markdown/)
```

This is rendered as:

[Link to Introductory article](/markdown/)

You can also link to a section in an article by specifying the section using an introducing hash character `#`.

For example, you can link to the section on "Links without description" in this same article by using:

```
[Link to the "links-without-description" section](#links-without-description)
```

This is rendered as:

[Link to the "links-without-description" section](#links-without-description)

You can link to another article's section using the same syntax.

For example, you can link to the section on "Bold" in the article "Introduction to Markdown" by using:

```
[Link to the bold section](/markdown/introduction-to-markdown/#bold)
```

This is rendered as:

[Link to the bold section](/markdown/introduction-to-markdown/#bold)