FROM node:12.22.12-stretch-slim

RUN apt-get update && apt-get -y upgrade

RUN apt-get install -y git

RUN mkdir /opt/documentation && chown node: /opt/documentation

USER node

WORKDIR /opt/documentation

RUN npm install

CMD [ "npm", "run", "serve"]